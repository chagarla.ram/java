import java.util.Scanner;

public class OnlineMain {
    public static void main(String args[]) {
        Owner owner = new Owner();
        Customer cust = new Customer();
        Scanner in = new Scanner(System.in);
        System.out.println("Enter are Customer/Owner? Customer/Owner");
        String s1 = in.nextLine();
        if (s1.equalsIgnoreCase("Owner")) {
            System.out.println("Hello Owner Welcome \n");
            owner.createClothes();
            owner.createAccessories();
            System.out.print(" **** Clothes ******* \n");
            owner.dispMapClothes(Users.getClhs());
            System.out.print(" **** Accessories ******* \n");
            owner.dispMapClothes(Users.getHs());
            System.out.println("Do you want add any new clothes/accessories y/n ?");
            if ((in.nextLine()).equals("y")) {
                System.out.println("What you want to add clothes/accessories ?");
                String si = in.nextLine();
                if (si.equals("clothes")) {
                    System.out.println("Enter Brand and clothes");
                    String data = in.nextLine();
                    owner.addClothes(data);
                    System.out.println("Thank you for adding! List after new update");
                    owner.dispMapClothes(Users.getClhs());
                } else if ((si.equals("accessories"))) {
                    System.out.println("Enter Brand and accessories");
                    String dat1 = in.nextLine();
                    owner.addAccessories(dat1);
                    System.out.println("Thank you for adding! List after new update");
                    owner.dispMapClothes(Users.getHs());
                }
            } else
                System.out.println("Thank you owner");
        }
        if (s1.equalsIgnoreCase("Customer")) {
            System.out.println("Shoppingcart is Empty");
            System.out.println("Do you want add item to shopping cart ? y/n");
            if (in.nextLine().equals("y")) {
                System.out.println("Enter item to be added");
                cust.addItem_To_ShoppingCart(in.nextLine());
                System.out.println("After adding item shopping cart");
                cust.dispMapClothes(Users.getShs());
            } else
                System.out.println("Thank you customer");
            System.out.println("remove item from shopping cart? y/n");
            if (in.nextLine().equals("y")) {
                System.out.println("Enter item to be removed");
                String s2 = in.nextLine();
                cust.removeItem_from_ShoppingCart(s2);
                System.out.println("After removing item shopping cart");
                cust.dispMapClothes(Users.getShs());
            } else
                System.out.println("Thank you customer");
        }
      }
    }

