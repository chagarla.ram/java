import java.util.*;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.Map;

public class Owner extends Users {

    private ArrayList<String> l = new ArrayList<String>() ;
    private ArrayList<String> l1 = new ArrayList<String>() ;
    private HashMap<String, ArrayList> hd = new HashMap<String, ArrayList>();

    public HashMap<String, ArrayList> createAccessories() {
        l.clear();
        hd=getHs();
        hd.clear();
        l.add("red_bag");
        l.add("blue_bag");
        l.add("green_bag");
        hd.put("puma", l);
        hd.put("lovely", l);
        hd.put("mantra", l);
        setHs(hd);
        return(hd);
    }
    protected void dispMapClothes(HashMap<String, ArrayList> l) {
        for (Map.Entry e : l.entrySet()) {
            System.out.println(e.getKey() + " " + e.getValue());
        }
    }

    public HashMap<String, ArrayList> createClothes() {
        hd=getClhs();
        hd.clear();
        l1.add("red_shirt");
        l1.add("blue_shirt");
        l1.add("green_shirt");
        l1.add("orange_shirt");
        l1.add("gray_shirt");
        hd.put("womenworld", l1);
        hd.put("varsha", l1);
        hd.put("vashva", l1);
        setClhs(hd);
        return(hd);
    }
    public void addClothes(String s) {
        hd = getClhs();
        String[] ts = 	s.split("\\s+");
        ArrayList<String> tmpstr = new ArrayList(Arrays.asList(ts));
        String brand= tmpstr.get(0);
        tmpstr.remove(0);
        hd.put(brand,tmpstr);
        setClhs(hd);
    }

    public void addAccessories(String s) {
        hd = getHs();
        String[] ts = 	s.split("\\s+");
        ArrayList<String> tmpstr = new ArrayList(Arrays.asList(ts));
        String brand= tmpstr.get(0);
        hd.put(brand, tmpstr);
        setHs(hd);
    }
}
