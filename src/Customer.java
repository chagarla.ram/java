import java.lang.reflect.Array;
import java.util.*;

public class Customer extends Users {
    private List<String> l = new ArrayList<>();
    private HashMap<String, ArrayList> hs;

    private static void convertStringToMap(String s, HashMap<String, ArrayList> l) {
        String[] ts = s.split("\\s+");
        ArrayList<String> tmpstr = new ArrayList(Arrays.asList(ts));
        String brand = ts[0];
        tmpstr.remove(0);
        if(getHs().containsValue(tmpstr.get(0)))
            l.put(brand, tmpstr);
    }

    public void addItem_To_ShoppingCart(String s) {
        convertStringToMap(s, getShs());
    }

    public void dispMapClothes(HashMap<String, ArrayList> l) {
        for (Map.Entry e : l.entrySet()) {
            System.out.println(e.getKey() + " " + e.getValue());
        }
    }

    public void removeItem_from_ShoppingCart(String s) {
        List<String> ts = Arrays.asList(s.split("\\s+"));
        String brand = ts.get(0);
        if (getShs().containsKey(brand) == true) {
            getShs().remove(brand);
        }
    }
}